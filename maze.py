import random
import msvcrt
import os
mymaze=[]
random_list=[1,1,0,0,0,0,0,0,0,0] #for wall density
mysize=int(input('Enter the size of maze= '))
def maze_generation(mysize):
    shortlist = []
    for x in range(mysize):
        for y in range(mysize):
            if y==0:shortlist.insert(y,1)
            elif y==mysize-1:shortlist.insert(y,1)
            else:
                a=random.choice(random_list)
                shortlist.insert(y,a)
        mymaze.insert(x,shortlist) #fixed the 9 rows problem
        shortlist=[]
    for x in range(mysize):shortlist.insert(0,1)
    mymaze[0]=shortlist
    mymaze[mysize-1]=shortlist
    mymaze[int((mysize-1)/2)][mysize-1]=0
def draw_maze():
    for x in range(len(mymaze)):
        shortlist = mymaze[x]
        for y in shortlist:
            if y == 1:print('\u2588\u2588', end='')
            elif y == '\u26F5':print('\u26F5 ', end='')
            else:print('  ', end='')
        print('')
maze_generation(mysize)
draw_maze()
x = int(1)
y = int(1)
mytrue=True
while mytrue==True:
    os.system('cls')
    mymaze[x][y]='\u26F5'
    draw_maze()
    if mymaze[int((mysize-1)/2)][mysize-1]=='\u26f5': # Are we at the exit?
        print('You Won!')
        mytrue=False
    i=(msvcrt.getch())#('Direction=\n'))
    if i== b'x':mytrue=False #ESC
    if i==b'M': #Move Right
        if mymaze[x][y+1] !=1: #Check Wall on right
            mymaze[x][y]=0
            y=y+1
    if i==b'P': #Move Down
        if mymaze[x+1][y]!=1: #Check Wall on down
            mymaze[x][y]=0
            x=x+1
    if i==b'H': #Move Up
        if mymaze[x-1][y]!=1: #Check Wall on up
            mymaze[x][y]=0
            x=x-1
    if i==b'K': #move Left
        if mymaze[x][y-1]!=1: #Check Wall on down
            mymaze[x][y] = 0
            y = y - 1